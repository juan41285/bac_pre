import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/presentacion',
    name: 'presentacion',
    meta: {titulo: 'Presentación'},
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/presentacion.vue')
  },
  {
    path: '/intro',
    name: 'intro',
    meta: { titulo: 'Introducción' },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/intro.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
